/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlestudiantil;

/**
 *
 * @author David
 */
public class AsignacionMateriaDocente {
    int id;
    int id_docente;
    int id_curso;
    String materia;
    String gestion;
    String ingreso;
    String salida;
    String turno;
    
    Curso curso;
    Docente docente;
    
    Conector conector= new Conector();

    public Curso getCurso() 
    {
        if(curso==null){
            curso = conector.obtenerCurso(this.id_curso);
        }
        return curso;
    }


    public Docente getDocente() 
    {
        if(docente==null)
        {
            docente = conector.obtenerDocente(this.id_docente);
        }
        return docente;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_docente() {
        return id_docente;
    }

    public void setId_docente(int id_docente) {
        this.id_docente = id_docente;
    }

    public int getId_curso() {
        return id_curso;
    }

    public void setId_curso(int id_curso) {
        this.id_curso = id_curso;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public String getGestion() {
        return gestion;
    }

    public void setGestion(String gestion) {
        this.gestion = gestion;
    }

    public String getIngreso() {
        return ingreso;
    }

    public void setIngreso(String ingreso) {
        this.ingreso = ingreso;
    }

    public String getSalida() {
        return salida;
    }

    public void setSalida(String salida) {
        this.salida = salida;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }
    
    
    
}
