/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlestudiantil;

/**
 *
 * @author David
 */
public class AsistenciaDocente {
    int id;
    int id_asignacion;
    int id_docente;
    String fecha;
    String ingreso;
    String salida;
    String turno;
    String observacion;
    
    Docente docente;
    AsignacionMateriaDocente asignacion_materia_Docente;
    Conector conector = new Conector();

    public Docente getDocente() 
    {
        if(docente==null)
        {
            docente = conector.obtenerDocente(this.id_docente);
        }
        return docente;
    }

    public AsignacionMateriaDocente getAsignacion_materia_Docente() 
    {
        if(asignacion_materia_Docente==null)
        {
           // asignacion_materia_Docente = conector.ob
        }
        return asignacion_materia_Docente;
    }
    
    
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_asignacion() {
        return id_asignacion;
    }

    public void setId_asignacion(int id_asignacion) {
        this.id_asignacion = id_asignacion;
    }

    public int getId_docente() {
        return id_docente;
    }

    public void setId_docente(int id_docente) {
        this.id_docente = id_docente;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getIngreso() {
        return ingreso;
    }

    public void setIngreso(String ingreso) {
        this.ingreso = ingreso;
    }

    public String getSalida() {
        return salida;
    }

    public void setSalida(String salida) {
        this.salida = salida;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    
}
