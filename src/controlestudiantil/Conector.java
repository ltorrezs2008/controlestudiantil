/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlestudiantil;

import java.util.ArrayList;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author David
 */
public class Conector {
    public static String BASE_DE_DATOS="vu1";
    public ArrayList<Estudiante> ListaDeEstudiantes()
    {
        ArrayList<Estudiante> lista = new ArrayList<>();
        
        String SQL_SELECT = "Select * from estudiantes order by id_estudiante";
        try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456");
             PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                int id = resultSet.getInt("id_estudiante");
                String nombres = resultSet.getString("nombre");
                String apellidos = resultSet.getString("apellidos");
                int telefono = resultSet.getInt("telefono");
                //
                Estudiante obj_estudiante = new Estudiante();
                obj_estudiante.setId(id);
                obj_estudiante.setApellidos(apellidos);
                obj_estudiante.setNombres(nombres);
                obj_estudiante.setTelefono(telefono);
                obj_estudiante.setGenero((String) resultSet.getString("genero"));
                obj_estudiante.setNivel((String) resultSet.getString("nivel"));
                lista.add(obj_estudiante);

            }
            lista.forEach(x -> System.out.println(x.nombres));

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    public void GuardarEstudiante(Estudiante estudiante)
    {
       if(estudiante.getId()==0) //quiere decir que si es un nuevo registro ver modelo en caso de dudas
       {
       
            try { 

                try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456")) {
                    Statement st = conn.createStatement();
                    st.executeUpdate("INSERT INTO estudiantes (nombre,apellidos,telefono,nivel,genero) " +
                            "VALUES ('"+estudiante.getNombres()+"', '"+estudiante.getApellidos()+"', "+estudiante.getTelefono()+",'"+estudiante.getNivel()+"','"+estudiante.getGenero()+"')");
                    // insert into estudiantes (nombre,apellidos,telefono,nivel,genero) values ("leyner","rata",123545,"primario","m";
                } 
            } catch (SQLException ex) {
                Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
            }
       }else
       {
           //en caso de que tenga id se tiene que actualizar solo ese registro
            try { 

                try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456")) {
                    Statement st = conn.createStatement();
                    st.executeUpdate("UPDATE estudiantes  SET nombre='"+estudiante.getNombres()+"',apellidos='"+estudiante.getApellidos()+"',telefono="+estudiante.getTelefono()+",nivel='"+estudiante.getNivel()+"',genero='"+estudiante.getGenero()+"'   WHERE id_estudiante="+estudiante.getId()+" ;");

                } 
            } catch (SQLException ex) {
                Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
            }
       }
    }
    
    public void EliminarEstudiante(Estudiante estudiante)
    {
        try { 

               try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456")) {
                   Statement st = conn.createStatement();
                   st.executeUpdate("DELETE FROM estudiantes  WHERE id_estudiante="+estudiante.getId()+" ;");
               } 
           } catch (SQLException ex) {
               Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
           }
    }
    
   
    
    public ArrayList<Docente> ListaDocentes()
    {
        ArrayList<Docente> lista = new ArrayList<>();
        
        String SQL_SELECT = "Select * from docentes order by id_docente";
        try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456");
             PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

               Docente docente= new Docente();
               
                docente.setId( (int) resultSet.getInt("id_docente") );
                docente.setNombre((String) resultSet.getString("nombre"));
                docente.setApellido((String) resultSet.getString("apellido"));
                docente.setSemestre((String) resultSet.getString("semestre"));
                docente.setCarrera((String) resultSet.getString("carrera"));
                docente.setRu((int) resultSet.getInt("ru"));
                docente.setTelefono((int) resultSet.getInt("telefono"));
                docente.setGenero((String) resultSet.getString("genero"));
                docente.setUniversidad((String) resultSet.getString("universidad"));
                lista.add(docente);

            }

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public ArrayList<AsignacionMateriaDocente> ListaAsignacionMateriaDocente()
    {
        ArrayList<AsignacionMateriaDocente> lista = new ArrayList<>();
        
        String SQL_SELECT = "Select * from asignacion_materia_docente order by id_asignacion";
         try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456");
             PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                 AsignacionMateriaDocente amd= new AsignacionMateriaDocente();
               
                amd.setId( (int) resultSet.getInt("id_asignacion") );
                amd.setId_curso((int) resultSet.getInt("id_curso") );
                amd.setId_docente((int) resultSet.getInt("id_docente") );
                amd.setMateria((String) resultSet.getString("materia"));
                amd.setGestion((String) resultSet.getString("gestion"));
                amd.setIngreso((String) resultSet.getString("ingreso"));
                amd.setSalida((String) resultSet.getString("salida"));
                amd.setTurno((String) resultSet.getString("turno"));
                lista.add(amd);

            }


        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }


    public ArrayList<Docente> ListaDeDocente()
    {
        ArrayList<Docente> lista = new ArrayList<>();
        
        String SQL_SELECT = "Select * from docentes order by id_docente";
        try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456");
             PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id_docente");
                String nombre = resultSet.getString("nombre");
                String apellidos = resultSet.getString("apellido");
                String carrera = resultSet.getString("carrera");
                String semestre = resultSet.getString("semestre");
                int ru = resultSet.getInt("ru");
                int telefono = resultSet.getInt("telefono");
                String genero = resultSet.getString("genero");
                String universidad = resultSet.getString("universidad");
                //
                Docente obj_docente = new Docente();
                obj_docente.setId(id);
                obj_docente.setNombre(nombre);
                obj_docente.setApellido(apellidos);
                obj_docente.setCarrera(carrera);
                obj_docente.setSemestre(semestre);
                obj_docente.setRu(ru);                
                obj_docente.setTelefono(telefono);
                obj_docente.setGenero((String) resultSet.getString("genero"));
                obj_docente.setUniversidad((String) resultSet.getString("Universidad"));
                lista.add(obj_docente);

            }
            lista.forEach(x -> System.out.println(x.nombre));

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public ArrayList<AsistenciaDocente> ListaAsistenciasDocente(int id_docente,int id_asignacion)
    {
        ArrayList<AsistenciaDocente> lista = new ArrayList<>();
        
        String SQL_SELECT = "Select * from asistencia_docentes  where id_docente="+id_docente+" and id_asignacion="+id_asignacion+" order by id_asistencia";
        try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456");
             PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) 
            {

               AsistenciaDocente asistencia = new AsistenciaDocente();
               
                asistencia.setId( (int) resultSet.getInt("id_asistencia") );
                asistencia.setId_asignacion((int) resultSet.getInt("id_asignacion") );
                asistencia.setId_docente((int) resultSet.getInt("id_docente") );
                asistencia.setFecha((String) resultSet.getString("fecha"));
                asistencia.setIngreso((String) resultSet.getString("ingreso"));
                asistencia.setSalida((String) resultSet.getString("salida"));
                asistencia.setTurno((String) resultSet.getString("turno"));
                asistencia.setObservacion((String) resultSet.getString("observacion"));

                lista.add(asistencia);

            }

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    public AsignacionMateriaDocente ObtenerAsignacionMateriaDocente(int id)
    {
        AsignacionMateriaDocente amd = new AsignacionMateriaDocente();
        
         String SQL_SELECT = "Select * from asignacion_materia_docente where id_asignacion="+id;
        try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456");
             PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
               
                amd.setId( (int) resultSet.getInt("id_asignacion") );
                amd.setId_curso((int) resultSet.getInt("id_curso") );
                amd.setId_docente((int) resultSet.getInt("id_docente") );
                amd.setMateria((String) resultSet.getString("materia"));
                amd.setGestion((String) resultSet.getString("gestion"));
                amd.setIngreso((String) resultSet.getString("ingreso"));
                amd.setSalida((String) resultSet.getString("salida"));
                amd.setTurno((String) resultSet.getString("turno"));
              

            }

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return amd;
    }
    
    public Curso obtenerCurso(int id)
    {
      
       Curso curso = new Curso();
       String SQL_SELECT = "Select * from cursos where id_curso="+id;
        try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456");
             PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                
                curso.setId((int) resultSet.getInt("id_curso"));
                curso.setNombre((String) resultSet.getString("nombre"));
                curso.setLugar((String) resultSet.getString("lugar"));
            }

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
       
       return curso;
    }


    public void GuardarDocente(Docente docente)
    {
       if(docente.getId()==0) //quiere decir que si es un nuevo registro ver modelo en caso de dudas
       {
       
            try { 

                try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456")) {
                    Statement st = conn.createStatement();
                    st.executeUpdate("INSERT INTO docentes (nombre,apellido,carrera,semestre,ru,telefono,genero,universidad) " +
                            "VALUES ('"+docente.getNombre()+"', '"+docente.getApellido()+"', '"+docente.getCarrera()+"', '"+docente.getSemestre()+"', "+docente.getRu()+", "+docente.getTelefono()+",'"+docente.getGenero()+"','"+docente.getUniversidad()+"')");
                    // insert into estudiantes (nombre,apellidos,telefono,nivel,genero) values ("leyner","rata",123545,"primario","m";
                } 
            } catch (SQLException ex) {
                Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
            }
       }else
       {
           //en caso de que tenga id se tiene que actualizar solo ese registro
            try { 

                try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456")) {
                    Statement st = conn.createStatement();
                    st.executeUpdate("UPDATE docentes  SET nombre='"+docente.getNombre()+"', apellido='"+docente.getApellido()+"', carrera='"+docente.getCarrera()+"', semestre='"+docente.getSemestre()+"', ru='"+docente.getRu()+"', telefono="+docente.getTelefono()+", genero='"+docente.getGenero()+"',universidad='"+docente.getUniversidad()+"'   WHERE id_docente="+docente.getId()+" ;");

                } 
            } catch (SQLException ex) {
                Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
            }
       }
    }
    
    public void EliminarDocente(Docente docente)
    {
        try { 

               try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456")) {
                   Statement st = conn.createStatement();
                   st.executeUpdate("DELETE FROM estudiantes  WHERE id_estudiante="+docente.getId()+" ;");
               } 
           } catch (SQLException ex) {
               Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
           }
    }

    public Docente obtenerDocente(int id)
    {
      
       Docente docente = new Docente();
       String SQL_SELECT = "Select * from docentes where id_docente="+id;

        try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456");
             PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

            docente.setId( (int) resultSet.getInt("id_docente") );
            docente.setNombre((String) resultSet.getString("nombre"));
            docente.setApellido((String) resultSet.getString("apellido"));
            docente.setSemestre((String) resultSet.getString("semestre"));
            docente.setCarrera((String) resultSet.getString("carrera"));
            docente.setRu((int) resultSet.getInt("ru"));
            docente.setTelefono((int) resultSet.getInt("telefono"));
            docente.setGenero((String) resultSet.getString("genero"));
            docente.setUniversidad((String) resultSet.getString("universidad"));
            }

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
       
       return docente;
    }
    
    
    public ArrayList<Docente> ListaDeDocentes()
    {
        ArrayList<Docente> lista = new ArrayList<>();
        
        String SQL_SELECT = "Select * from docentes order by id_docente";

        try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456");
             PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Docente docente = new Docente();
                docente.setId( (int) resultSet.getInt("id_docente") );
                docente.setNombre((String) resultSet.getString("nombre"));
                docente.setApellido((String) resultSet.getString("apellido"));
                docente.setSemestre((String) resultSet.getString("semestre"));
                docente.setCarrera((String) resultSet.getString("carrera"));
                docente.setRu((int) resultSet.getInt("ru"));
                docente.setTelefono((int) resultSet.getInt("telefono"));
                docente.setGenero((String) resultSet.getString("genero"));
                docente.setUniversidad((String) resultSet.getString("universidad"));
                lista.add(docente);

            }

              } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
     public ArrayList<Curso> ListaCursos()
    {
        ArrayList<Curso> lista = new ArrayList<>();
        
        String SQL_SELECT = "Select * from cursos order by id_curso";

        try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456");
             PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Curso curso = new Curso();
                curso.setId( (int) resultSet.getInt("id_curso"));
                curso.setNombre((String) resultSet.getString("nombre"));
                curso.setLugar((String) resultSet.getString("lugar"));
               
                lista.add(curso);

            }

              } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }
    
    public Estudiante obtenerEstudiante(int id)
    {
      
       Estudiante estudiante = new Estudiante();
       String SQL_SELECT = "Select * from estudiantes where id_estudiante="+id;
        try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456");
             PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                
                estudiante.setId( (int) resultSet.getInt("id_estudiante") );
                estudiante.setNombres((String) resultSet.getString("nombre"));
                estudiante.setApellidos((String) resultSet.getString("apellidos"));
                estudiante.setTelefono((int) resultSet.getInt("telefono"));
                estudiante.setGenero((String) resultSet.getString("genero"));
                estudiante.setNivel((String) resultSet.getString("nivel"));
            }

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return estudiante;
    }
    
    public void RegistrarAsistencia(AsistenciaDocente asistencia)
    {
       if(asistencia.getId()==0) //quiere decir que si es un nuevo registro ver modelo en caso de dudas
       {
       
            try { 

                try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456")) {
                    Statement st = conn.createStatement();
                    st.executeUpdate("INSERT INTO asistencia_docentes (id_asignacion,id_docente,fecha,ingreso,salida,turno,observacion) " +
                            "VALUES ("+asistencia.getId_asignacion()+","+asistencia.getId_docente()+",'"+asistencia.getFecha()+"','"+asistencia.getIngreso()+"','"+asistencia.getSalida()+"','"+asistencia.getTurno()+"','"+asistencia.getObservacion()+"' )");
                    // insert into estudiantes (nombre,apellidos,telefono,nivel,genero) values ("leyner","rata",123545,"primario","m";
                } 
            } catch (SQLException ex) {
                Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
            }
       }else
       {
           //en caso de que tenga id se tiene que actualizar solo ese registro
           /*
            try { 

                try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456")) {
                    Statement st = conn.createStatement();
                    st.executeUpdate("UPDATE docentes  SET nombre='"+docente.getNombre()+"', apellido='"+docente.getApellido()+"', carrera='"+docente.getCarrera()+"', semestre='"+docente.getSemestre()+"', ru='"+docente.getRu()+"', telefono="+docente.getTelefono()+", genero='"+docente.getGenero()+"',universidad='"+docente.getUniversidad()+"'   WHERE id_docente="+docente.getId()+" ;");

                } 
            } catch (SQLException ex) {
                Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
            }
            */
       }
    }
    public void RegistrarCurso(Curso curso)
    {
       if(curso.getId()==0) //quiere decir que si es un nuevo registro ver modelo en caso de dudas
       {
       
            try { 

                try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456")) {
                    Statement st = conn.createStatement();
                    st.executeUpdate("INSERT INTO cursos (nombre,lugar) " +
                            "VALUES ('"+curso.getNombre()+"','"+curso.getLugar()+"' )");
                    // insert into estudiantes (nombre,apellidos,telefono,nivel,genero) values ("leyner","rata",123545,"primario","m";
                } 
            } catch (SQLException ex) {
                Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
            }
       }else
       {
           //en caso de que tenga id se tiene que actualizar solo ese registro
           /*
            try { 

                try (Connection conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/"+BASE_DE_DATOS, "postgres", "123456")) {
                    Statement st = conn.createStatement();
                    st.executeUpdate("UPDATE docentes  SET nombre='"+docente.getNombre()+"', apellido='"+docente.getApellido()+"', carrera='"+docente.getCarrera()+"', semestre='"+docente.getSemestre()+"', ru='"+docente.getRu()+"', telefono="+docente.getTelefono()+", genero='"+docente.getGenero()+"',universidad='"+docente.getUniversidad()+"'   WHERE id_docente="+docente.getId()+" ;");

                } 
            } catch (SQLException ex) {
                Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
            }
            */
       }
    }
}
