/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlestudiantil;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author dilan
 */
public final class PanelDocentes extends javax.swing.JPanel {

    /**
     * Creates new form PanelDocentes
     */
    ArrayList<Docente> lista_de_docentes;
    Conector conector_base;
    FormularioDocente formulario_docente;
    
    public PanelDocentes() {
        initComponents();
        conector_base = new Conector();        
       
        //LimpiarTabla();
        ObtenerDocentes();
    }

    
    public void NuevoDocente()
    {
        this.formulario_docente = null;
        formulario_docente = new FormularioDocente(this);
        formulario_docente.setDocente(new Docente());
        formulario_docente.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                //formulario_estudiante.setVisible(false);
                //actualizar lista de estudiantes
                ObtenerDocentes();
                formulario_docente.setVisible(false);
                /*
                if (JOptionPane.showConfirmDialog(formulario_estudiante, 
                    "Are you sure you want to close this window?", "Close Window?", 
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
                    System.exit(0);
                }*/
            }

           
            
        });
        formulario_docente.setVisible(true);
    }
    public void EditarDocente(int index)
    {
        Docente docente =(Docente) lista_de_docentes.get(index);
        this.formulario_docente = null;
        this.formulario_docente = new FormularioDocente(this);
        this.formulario_docente.setDocente(docente);
        this.formulario_docente.setVisible(true);
    }
    public void ObtenerDocentes()
    {
        
        this.lista_de_docentes = conector_base.ListaDeDocente();
       
        String col[] = {"Nro","Nombre","Apellido","Carrera","Semestre","R.U.:", "Telefono","Genero","Universidad",};

        DefaultTableModel model = new DefaultTableModel(col, 0){
            @Override
            public boolean isCellEditable(int row, int column) {
               //all cells false
               return false;
            }
        };
        jTable1.setModel(model);
        
       
       // DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        lista_de_docentes.forEach((docente) -> {
            model.addRow(new Object[]{docente.getId(), docente.getNombre(),docente.getApellido(),docente.getCarrera(),docente.getSemestre(),docente.getRu(),docente.getTelefono(),docente.getGenero(),docente.getUniversidad()});
        });
        
        /*for(int i=0;i<this.lista_de_estudiantes.size();i++){
            Estudiante estudiante = (Estudiante) this.lista_de_estudiantes.get(i);
             model.addRow(new Object[]{estudiante.getId(), estudiante.getNombres(),estudiante.getApellidos(),estudiante.getTelefono(),estudiante.getNivel(),estudiante.getGenero()});
        }*/
        
        jTable1.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                JTable table =(JTable) mouseEvent.getSource();
                Point point = mouseEvent.getPoint();
                int index_docente = table.rowAtPoint(point);
                if (mouseEvent.getClickCount() == 2 && !mouseEvent.isConsumed()) {
                    // your valueChanged overridden method 
                     mouseEvent.consume();
                    System.out.println("Fila : "+index_docente);
                    EditarDocente(index_docente);
                    
                }
            }
        });
        
    
        jTable1.validate();
        jTable1.repaint();
       
    }
    public void LimpiarTabla()
    {
         String col[] = {"Nro","Nombre","Apellido", "Carrera" ,"Semestre" ,"RU","Telefono","Genero","Universidad"};

        DefaultTableModel tableModel = new DefaultTableModel(col, 0);
        jTable1.setModel(tableModel);
     
    }
    public void RegistrarDocente(Docente docente)
    {
        System.out.println("Registrando "+docente.getNombre());
        conector_base.GuardarDocente(docente);
        this.formulario_docente.dispose();
        this.formulario_docente =null;
        ObtenerDocentes();
    }
    
    public void BorrarDocente(Docente docente)
    {
        System.out.println("Eliminando "+docente.getNombre());
        conector_base.EliminarDocente(docente);
        this.formulario_docente.dispose();
        this.formulario_docente = null;
        ObtenerDocentes();
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Docentes");

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/controlestudiantil/images/refresh_black.png"))); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/controlestudiantil/images/person_black.png"))); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jScrollPane1.setPreferredSize(new java.awt.Dimension(472, 402));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 271, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton3)
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 451, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton3)
                    .addComponent(jButton2)
                    .addComponent(jLabel2))
                .addContainerGap(332, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap(47, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 312, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        ObtenerDocentes();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        NuevoDocente();
    }//GEN-LAST:event_jButton3ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
