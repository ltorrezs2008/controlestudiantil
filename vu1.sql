CREATE TABLE Estudiantes(
	id_estudiante serial  primary key,
	nombre VARCHAR(20),
	apellidos VARCHAR(60),
	telefono INTEGER,
	nivel VARCHAR(20),
	genero VARCHAR(1)
);

CREATE TABLE Cursos(
	id_curso serial  primary key,
	nombre VARCHAR(60),
	lugar VARCHAR(60)
);


CREATE TABLE Docentes(
	id_docente serial  primary key,
	nombre VARCHAR(20),
	apellido VARCHAR(60),
	semestre VARCHAR(20),
	carrera VARCHAR(60),
	ru INTEGER,
	telefono INTEGER,
	genero VARCHAR(1),
	universidad VARCHAR(100)
);

CREATE TABLE Asignacion_Materia_Docente(
	id_asignacion serial  primary key,
	id_docente INTEGER,
	id_curso INTEGER,
	materia VARCHAR(20),
	gestion VARCHAR(20),
	ingreso TIME,
	salida   TIME,
	turno	VARCHAR(20)
);

CREATE TABLE Inscritos(
	id_inscrito serial  primary key,
	id_estudiante INTEGER,
	id_asignacion INTEGER,
	fecha_pago TIME,
	monto_pago INTEGER,
	validación VARCHAR(2)
	
);


CREATE TABLE Asistencia_Docentes(
	id_asistencia serial  primary key,
	id_asignacion INTEGER,
	id_docente INTEGER,
	fecha DATE,
	ingreso TIME,
	salida TIME,
	turno VARCHAR(20),
	observación VARCHAR(200)
);

ALTER TABLE Asignacion_Materia_Docente
   ADD CONSTRAINT FK_asignacion_docente
   FOREIGN KEY (id_docente)
   REFERENCES Docentes(id_docente);

ALTER TABLE Asignacion_Materia_Docente
   ADD CONSTRAINT FK_asignacion_curso
   FOREIGN KEY (id_curso)
   REFERENCES Cursos(id_curso);

ALTER TABLE Inscritos
   ADD CONSTRAINT FK_inscritos_estudiante
   FOREIGN KEY (id_estudiante)
   REFERENCES Estudiantes(id_estudiante);

ALTER TABLE Inscritos
   ADD CONSTRAINT FK_inscritos_asignacion
   FOREIGN KEY (id_asignacion)
   REFERENCES asignacion_materia_docente(id_asignacion);

ALTER TABLE Asistencia_docentes
	ADD CONSTRAINT FK_asistencia_asignacion
	FOREIGN KEY (id_asignacion)
	REFERENCES asignacion_materia_docente (id_asignacion);

ALTER TABLE Asistencia_docentes
	ADD CONSTRAINT FK_asistencia_docente
	FOREIGN KEY (id_docente)
	REFERENCES docentes (id_docente);
